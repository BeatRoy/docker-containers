#!/bin/bash
if [ ! -f "manage.py" ]; 
then
django-admin startproject myapp /django
fi

echo "Apply database migrations"
python manage.py migrate --no-input

echo "Starting server"
python manage.py runserver 0.0.0.0:1080
